//arquivo conf.js

exports.config = {

   seleniumAddress: 'http://localhost:4444/wd/hub',//onde o selenium está rodando
    specs: ['features/*.feature'],
    baseUrl: "http://www.way2automation.com",
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    cucumberOpts: {
        require: [
            './step_definitions/*.step.js',
        ],
        format: ['json:results.json'],
        profile: false,
        'no-source': true,
    },
    
    afterLaunch: function () {
        var reporter = require('cucumber-html-reporter');

        var options = {
            theme: 'bootstrap',
            jsonFile: 'results.json',
            output: './tests_result/cucumber_report.html',
            reportSuiteAsScenarios: true,
            launchReport: true
        };

        reporter.generate(options);

    },
   
    
    onPrepare: function () {
        const {Given, Then, When, Before} = require('cucumber');
        global.Given = Given;
        global.When = When;
        global.Then = Then;
        global.Before = Before;
      },   

      allScriptsTimeout: 10000,


    }

  /*
        var AllureReporter = require('Jasmine-allure-reporter');
        jasmine.getEnv().addReporter(new AllureReporter({
            resultsDir: 'allure-results'

        }));

       
        
            summary: {
                displayErrorMessages: true,
                displayStacktrace: false,
                displaySuccessfull: true,
                displayFailed: true,
                displayDuration: true
            },
            colors: {
                enabled: true
            }

        }));*/
   // },


    /*,capabilities:{
        //alterando o navegador default para execução dos testes
        browserName:"Firefox"
    }
    //configurando para rodar os teste em mais de um browser
    , multicapabilities:[{
        browserName:'chrome'},
    { 
        browserName:'Firefox'}]*/
