/**
* @file loginpage.po.js
*/

var LoginPage = function () {

  this.user = element(by.id('username'));
  this.pass = element(by.id('password'));
  // this.username=element(by.xpath('//*[@id="formly_1_input_username_0"]'));
  this.username = element(by.model('model[options.key]'));
  this.successMessage = element(by.xpath('/html/body/div[3]/div/div/div/p[1]'));
  this.loginButton = element(by.className('btn-danger'));
  this.incorrectCredentials = element(by.className('alert-danger'));
  this.logoutLink = element(by.linkText('Logout'));

  this.get = function () {
    browser.get('/angularjs-protractor/registeration/#/login');
  };

  this.fillForm = function (user, pass, username) {
    this.user.sendKeys(user);
    this.pass.sendKeys(pass);
    this.username.sendKeys(username);
  };

  this.login = function (user, pass, username) {
    this.fillForm(user, pass, username);
    this.loginButton.click();
  };

  this.logout = function(){
    this.logoutLink.click();
  }

};

module.exports = new LoginPage();