//specs.js

let LoginPage = require('../pages/loginpage.po.js');

let chai = require('chai');
let chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;

browser.ignoreSynchronization = false;
/*
beforeEach(function () {
    LoginPage.get();

});*/


  Given('que eu estou na tela de login', function () {
    LoginPage.get();
  });

  When('eu digito o usuario {string}, senha {string} e Nome {string}', function (string, string2, string3) {
    LoginPage.login(string, string2, string3);
});

  Then('o sistema realiza o login com sucesso', function () {
    expect(LoginPage.successMessage.isDisplayed()).toBe(true);

});